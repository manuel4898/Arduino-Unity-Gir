﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;

public class Arduino : MonoBehaviour
{
    string[] puertos;
    SerialPort arduinoUno;
    public Dropdown lista_puertos;
    string puerto_seleccionado;
    bool conected;
    public GameObject cubo;


    private void Awake()
    {
        lista_puertos.options.Clear();
        puertos = SerialPort.GetPortNames();

        foreach (string port in puertos)
        {
            lista_puertos.options.Add(new Dropdown.OptionData() { text = port });
        }
        DropdownItemSelected(lista_puertos);
        lista_puertos.onValueChanged.AddListener(delegate { DropdownItemSelected(lista_puertos); });
        Debug.Log(puertos);
    }
    public void Conectar()
    {
        if (!conected)
        {
            arduinoUno = new SerialPort(puerto_seleccionado, 9600, Parity.None, 8, StopBits.One);
            arduinoUno.Open();
            Debug.Log("Conectado");

            conected = true;
        }
    }
    public void Desconectar()
    {
        if (conected)
        {
            arduinoUno.Close();
            Debug.Log("Desconectado");

            conected = false;
        }
    }

    public void DropdownItemSelected(Dropdown lista) {
        int indice = lista.value;
        puerto_seleccionado = lista_puertos.options[indice].text;
        Debug.Log(puerto_seleccionado);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (conected)
        {
            string resultado = arduinoUno.ReadLine();
            // String of authors  
            string[] numbers = resultado.Split(';');



            int a_z = int.Parse(numbers[0]);
            int a_x = int.Parse(numbers[1]);
            int a_y = int.Parse(numbers[2]);

            Debug.Log(resultado);

            cubo.transform.rotation = Quaternion.Euler(a_x, a_y, a_z);
        }
    }
}
